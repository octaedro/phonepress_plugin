<?php

require_once dirname(__FILE__ ) . '/class-pnfw-api-registered.php';

class PW2MAAPI_Unregister extends PW2MAAPI_Registered {

 public function __construct() {
  parent::__construct(home_url('pnfw/unregister/'), 'POST');

  global $wpdb;
  $push_tokens = $wpdb->get_blog_prefix() . 'push_tokens';

  $user_id = $wpdb->get_var($wpdb->prepare("SELECT user_id FROM $push_tokens WHERE token = %s AND os = %s", $this->token, $this->os));

  $res = $wpdb->delete($push_tokens, array("token" => $this->token, "os" => $this->os));

  if ($res === false) {
   $this->json_error('500', __('Unable to delete token', 'push-notifications-for-wp'));
  }

  $user = new WP_User($user_id);

  if (in_array(Phonepress_plugin_for_wordpress_Lite::USER_ROLE, $user->roles) && empty($user->user_email)) {
   pw2ma_log(PW2MASYSTEM_LOG, sprintf(__("Automatically deleted the anonymous user %s (%s) since left without tokens.", 'push-notifications-for-wp'), $user->user_login, $user_id));
   if (is_multisite()) {
    if (is_user_member_of_blog($user_id)) {
     require_once(ABSPATH . 'wp-admin/includes/ms.php');
     wpmu_delete_user($user_id);
    }
   }
   else {
    require_once(ABSPATH . 'wp-admin/includes/user.php');
    wp_delete_user($user_id);
   }
  }

  exit;
 }
}
