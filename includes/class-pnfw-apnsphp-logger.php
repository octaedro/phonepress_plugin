<?php

if (!defined('ABSPATH')) {
 exit; // Exit if accessed directly
}

class PW2MAApnsPHP_Logger implements ApnsPHP_Log_Interface {

 protected $type;

 public function __construct($type) {
  $this->type = $type;
 }

 public function log($sMessage) {
  pw2ma_log($this->type, $sMessage);
 }

}
