<?php

// if uninstall not called from WordPress exit
if (!defined('WP_UNINSTALL_PLUGIN'))
 exit();

function pw2ma_delete_plugin() {
 global $wpdb;

 $table_name = $wpdb->get_blog_prefix() . 'push_tokens';
 $wpdb->query("DROP TABLE IF EXISTS $table_name;");

 $table_name = $wpdb->get_blog_prefix() . 'push_viewed';
 $wpdb->query("DROP TABLE IF EXISTS $table_name;");

 $table_name = $wpdb->get_blog_prefix() . 'push_sent';
 $wpdb->query("DROP TABLE IF EXISTS $table_name;");

 $table_name = $wpdb->get_blog_prefix() . 'push_excluded_categories';
 $wpdb->query("DROP TABLE IF EXISTS $table_name;");

 $table_name = $wpdb->get_blog_prefix() . 'push_logs';
 $wpdb->query("DROP TABLE IF EXISTS $table_name;");

 $table_name = $wpdb->get_blog_prefix() . 'push_encryption_keys';
 $wpdb->query("DROP TABLE IF EXISTS $table_name;");

 $table_name = $wpdb->get_blog_prefix() . 'postmeta';
 $wpdb->query("DELETE FROM $table_name WHERE meta_key = 'pw2ma_do_not_send_push_notifications_for_this_post' OR meta_key = 'pw2ma_user_cat';");

 $user_query = new WP_User_Query(array('role' => 'app_subscriber'));

 foreach ($user_query->results as $user) {
  if (empty($user->user_email)) {
   if (is_multisite()) {
    if (is_user_member_of_blog($user->ID)) {
     require_once(ABSPATH . 'wp-admin/includes/ms.php');
     wpmu_delete_user($user->ID);
    }
   }
   else {
    wp_delete_user($user->ID);
   }
  }
 }
 delete_option('pw2ma_db_version');
 delete_option('pw2ma_posts_per_page');
 delete_option('pw2ma_last_save_timestamp');
 delete_option('pw2ma_enable_push_notifications');
 delete_option('pw2ma_ios_push_notifications');
 delete_option('pw2ma_android_push_notifications');
 delete_option('pw2ma_kindle_push_notifications');
 delete_option('pw2ma_url_scheme');
 delete_option('pw2ma_ios_use_sandbox');
 delete_option('pw2ma_sandbox_ssl_certificate_media_id');
 delete_option('pw2ma_sandbox_ssl_certificate_password');
 delete_option('pw2ma_production_ssl_certificate_media_id');
 delete_option('pw2ma_production_ssl_certificate_password');
 delete_option('pw2ma_ios_payload_sound');
 delete_option('pw2ma_google_api_key');
 delete_option('pw2ma_adm_client_id');
 delete_option('pw2ma_adm_client_secret');
 delete_option('pw2ma_api_consumer_key');
 delete_option('pw2ma_api_consumer_secret');
 delete_option('pw2ma_api_oauth_relax');
 delete_option('pw2ma_enabled_post_types');
 delete_option('pw2ma_enabled_object_taxonomies');
 delete_option('pw2ma_use_wpautop');
 delete_option('pw2ma_disable_email_verification');
 delete_option('pw2ma_use_multiple_background_processes');
 delete_option('pw2ma_add_message_field_in_payload');
 delete_option('pw2ma_uninstall_data');

 flush_rewrite_rules();
}

if (get_option('pw2ma_uninstall_data')) {
 pw2ma_delete_plugin();
}
